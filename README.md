Codes relating to the article "A mechanistic-statistical approach to infer dispersal and demography from invasion dynamics, applied to a plant pathogen".

These codes are designed to infer the dispersal of a species along a one dimensional domain. The method is then applied to real datasets of observed epidemiologic data of the invasion of the poplar rust pathogen, *Melampsora larici-populina*, along the Durance river valley. 

A first description of each script is provided by alphabetical order. Then, a description of each provided file is given by alphabetical order. Finally, a more general description of the project is provided with the order in which the scripts must be executed to obtain the figures and data presented in the article.

# Script Descriptions

1. `Estimation_Table1.R`
	
	Calculate the parameter practical identifiability for each chosen dispersal, to produce the necessary results to fill Table 1 of the article

	**Inputs:** 
	- Number of distinct growth rates along the invaded domain
	- List of dispersal to choose as simulated data
	- Number of replicates
	- Space step for the space discretisation
	- Time step for the time discretisation
	- Number of simulations for the matrix of initial conditions, if is not already created
	- Boolean indicating if the real datase is used
	- Boolean indicating if the mean number of leaves observed by tree vary from one site to the other
	- A vector of fixed parameters describing the sampling
	- Initial conditions of the epidemic (Tableau_Conditions_Init.txt)

	**Outputs:**
	- Results of the parameter practical identifiability for the four dispersals (.csv)

2. `Estimation_Table2.R`
	
	Calculate the efficiency of model selection, to produce the necessary results to fill Table 2 of the article

	**Inputs:** 
	- Number of distinct growth rates along the invaded domain
	- List of dispersal to choose as simulated data
	- List of dispersal to re-estimate based on the simulated data
	- Number of replicates
	- Space step for the space discretisation
	- Time step for the time discretisation
	- Number of simulations for the matrix of initial conditions, if is not already created
	- Boolean indicating if the real datase is used
	- Boolean indicating if the mean number of leaves observed by tree vary from one site to the other
	- A vector of fixed parameters describing the sampling
	- Initial conditions of the epidemic (Tableau_Conditions_Init.txt)

	**Outputs:**
	- Results of the efficiency of model selection for the four dispersals (.csv)

3. `Estimation_Table3.R`
	
	Model choice applied to the real dataset, to produce the necessary results to fill Table 3 of the article

	**Inputs:** 
	- Number of distinct growth rates along the invaded domain
	- List of dispersal to test on the real data
	- Number of replicates
	- Space step for the space discretisation
	- Time step for the time discretisation
	- Number of simulations for the matrix of initial parameter values, if is not already created
	- Boolean indicating if the real datase is used
	- Boolean indicating if the mean number of leaves observed by tree vary from one site to the other
	- A vector of fixed parameters describing the sampling
	- Real datasets for the two observation types: Tree_Data.txt and Twig_Data.txt
	- Initial conditions of the epidemic (Tableau_Conditions_Init.txt)

	**Outputs:**
	- Table of estimated dispersal and parameter for each replicate, with the corresponding AIC (Table3.csv)

4. `Fct-LoiGammaBinomiale.R`
	
	Functions used to calculate and use the Gamma Binomial distributions

5. `Fct-Resolution-Euler-gen-rcppeigen.cpp`
	
	Functions used to resolve Euler numerical scheme for each dispersal

6. `Fct-Vraisemblance.R`
	
	Functions used to calculate the loglikelihood of parameter combinations, knowing the tree and/or twig dataset

7. `Graphics_Tables_Display.R`
	
	Display Tables and Figures as presented in the article

	**Inputs:** 
	- Results from `Estimation_Table1.R`
	- Results from `Estimation_Table2.R`
	- Results from `Estimation_Table3.R`
	- Space step for the space discretisation
	- Time step for the time discretisation
	- Real datasets (Tree_Data.txt and Twig_Data.txt)
	- Initial conditions of the epidemic (Tableau_Conditions_Init.txt)
	- Boolean indicating if the real datase is used
	- Boolean indicating if the mean number of leaves observed by tree vary from one site to the other
	- A vector of fixed parameters describing the sampling
	- Tables of initial parameter values for the optimisation (Matrix_CI_Exp.csv, Matrix_CI_ExpPuiss.csv, Matrix_CI_Gauss.csv, Matrix_CI_RD.csv)

	**Outputs:**
	- Table 1 and Figures S2, S3, S4, S5. Parameters practical identifiatility, with graphics of correlation between true and estimated values
	- Table 2, Table S2 and Figure 2. Efficiency of model selection
	- Table 3, Table 4 and Figure 5. Case study: Model selection for the epidemic of poplar rust
	- Figure 4. Distribution of the number of infected leaves in a tree and the number of infected leaves in a twig for the inferred parameter values
	- Figure S1. Distribution of parameters before and after retaining only parameter values leading to realistic epidemics

# Provided Files Description

1. Matrix_CI_Exp.csv, Matrix_CI_ExpPuiss.csv, Matrix_CI_Gauss.csv, Matrix_CI_RD.csv

	Stored in file Initial_Conditions_Optim, these tables represent 500 parameter combinations leading to realistic epidemics for each dispersal. These files represent the matrices of initial parameter values used for the optimisation in the article and are thus profided for reproducibility.

2. Table1_Exp.csv, Table1_ExpPuiss.csv, Table1_Gauss.csv, Table1_RD.csv

	Results from `Estimation_Table1.R` obtained for the article, provided for reproducibility of the tables and figures.

3. Table2_densifFALSE_Exp.csv, Table2_densifFALSE_ExpPuiss.csv, Table2_densifFALSE_Gauss.csv, Table2_densifFALSE_RD.csv, Table2_densifTRUE_Exp.csv, Table2_densifTRUE_ExpPuiss.csv, Table2_densifTRUE_Gauss.csv, Table2_densifTRUE_RD.csv

	Results from `Estimation_Table2.R` obtained for the article, provided for reproducibility of the tables and figures.

4. Table3.csv

	Results from `Estimation_Table3.R` obtained for the article, provided for reproducibility of the tables and figures.

5. Tableau_Conditions_Init.txt

	Stored in the folder RealData, initial population densities taken as initial conditions of the epidemics, with the infectious potential along the simulated domain. These initial conditions result from the first temporal sampling of the poplar rust disease in the Durance river valley, and constitue the starting point of all simulated epidemics.

6. Tree_Data.txt

	Stored in the folder RealData, real dataset of the tree sampling to monitor poplar rust infections along the Durance river valley. 

	- date: the number of the sampling date (from 2 to 7, sampling 1 beeing used for the initial conditions of the epidemics) 
	- distance: the distance from the site upstream, in km
	- Nfeuil: the number of infected trees observed
	- Narbre: the total number of trees observed
	- site: the number of the sampling site
	- jour: the number of days since the start of the epidemics
	- M: the mean number of observed leaves per tree
	- IsEch1: boolean that indicates if the tree sampling was performed (1) or not (0) at this date in this site

7. Twig_Data.txt

	Stored in the folder RealData, real dataset of the twig sampling to monitor poplar rust infections along the Durance river valley. 

	- date: the number of the sampling date (from 2 to 7, sampling 1 beeing used for the initial conditions of the epidemics) 
	- jour: the number of days since the start of the epidemics
	- site: the number of the sampling site
	- distance: the distance from the site upstream, in km
	- IsEch2: boolean that indicates if the twig sampling was performed (1) or not (0) at this date in this site
	- rameau: the number of the observed twig
	- NFeuilTotal: the total number of leaves observed on this twig
	- NFeuilInf: the number of infected leaves on this twig

# General Code Description

To obtain the results presented in the article, scripts must be run in the following order without parameter modifications. Please note that some scripts can take a long time to be executed (from several hours to several days on a computer with 32Go RAM, Intel(R) Xeon(R) W-1390P @ 3.50GHz), they are indicated as **time-consuming** in the following description. Tables of initial parameter values for the optimisation used in the article are provided for reproducibility. For each script to run, choose as working directory your source file location.

Run first the analyses providing the parameters practical identifiability (`Estimation_Table1.R`, **time-consuming**: 244 h), the dispersal identifiability (`Estimation_Table2.R`, **time-consuming**: 327 h) and performs the application of the inference to the real datasets (`Estimation_Table3.R`, 3h). These three scripts can be run independently. Please not that this step is optionnal, because for reproducibility of the tables and figures presented in the article, all the results obtained from these three codes are already provided in the Results folder.

Then, run `Graphics_Tables_Display.R` to obtain the information, figures and tables as presented in the article.

R and package versions for R scripts:

	- R version 4.2.2 (2022-10-31)
	- gridExtra_2.3
	- ggplot2_3.3.5
	- dfoptim_2020.10-1
	- emdbook_1.3.12
	- matrixStats_0.61.0 
	- RcppEigen_0.3.3.9.2
	- Rcpp_1.0.8.3
	- EnvStats_2.7.0
	- dplyr_1.0.8
	- dfoptim_2020.10-1
	- KScorrect_1.4.0
	- mvtnorm_1.1-3
	- bbmle_1.0.24
	- actuar_3.2-2
	- reshape2_1.4.4
	- countreg_0.2-1
	- MASS_7.3-58.1

